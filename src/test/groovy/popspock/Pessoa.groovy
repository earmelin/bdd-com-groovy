package popspock

class Pessoa {
	String nome
	int idade
	
	static void main(args) {
		println("ola groovy")
		
		def pessoinha = new Pessoa(nome:'edu', idade: 40)
		println("o nome dele é ${pessoinha.nome}")
		
		pessoinha.setNome('Jose')

		println("o nome dele é ${pessoinha.nome}")
		println("o nome dele é ${pessoinha.getNome()}")
		
		println("""
			Seu amor me pegou
			que tiro foi esse
			""")
		def cores = ['azul', 'verde', 'branco']
		println(cores[0])
		println(cores[-1])
		
		println(cores.first())
		println(cores.last())
		
		println('---o---')
		
		cores.each { 
			println("cor: ${it}")				
		}
		println('---1---')
		
		cores.each { cor ->
			println("cor: ${cor}")				
		}
		
		//
		10.times { println("abacate") }
		
		if (cores) {
			println("cores ok  - nao é nulo nem empty")
		}
		
		
		def apelido = 'zeze'
		if (apelido) {
			println('existe apelido.')
		}
		
		apelido = null
		if (apelido) {
			println('existe aplelido')
		}

		def filhos = 5
		if (filhos) {
			println("tem filhos.")
		}
		
		filhos = 0
		if (filhos) {
			println("tem filhos")
		}

		List bairros = null
		println(bairros?.first())
		
		bairros = ['cidade tiradentes', 'guaianazes']
		println(bairros?.first())
				
	}
}	
