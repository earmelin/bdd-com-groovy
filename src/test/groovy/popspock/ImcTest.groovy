package popspock

import br.itau.grove.popspock.calculadoraIMC
import spock.lang.Specification

class ImcTest extends Specification {
	
	def 'deve validar o imc do cliente'() { 
		
	given: 'receber peso do cliente'
	def peso1 = 70
	def peso2 = 100
	
	when: 'verificar o peso'
	def calcularIMC = new calculadoraIMC()
	def verificacao1 = calcularIMC.verificaSaude(peso1)
	def verificacao2 = calcularIMC.verificaSaude(peso2)
	
	then: 'deve aprovar o IMC menor que 80'
	verificacao1 &&	!verificacao2
	
	}
		
}
