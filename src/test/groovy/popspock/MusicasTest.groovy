package popspock

import spock.lang.Specification

class MusicasTest extends Specification{
	
	def 'deve cantar a musica certa'() {
		given: 'receber o nome da moça'
		def mina = 'Jeniferc' //'Estella'
		
		when: 'cante a musica' 
		def musica = "o nome dela é $mina"
		
		then: 'a musica deve ser sobre Jenifer'
		musica == "o nome dela é Jenifer"
	}
	
}
